## Usage

### Prerequisites

Make sure that you have Node and NPM installed.

### Installing

1- Clone this repository using

```
git clone https://bitbucket.org/burakyagdiran/perf-analytics
```

2- Go to project folder with

```
cd perf-analytics
```

3- Install projects modules with

```
npm run setup-api
npm run setup-client
```

4- Start projects with

```
npm run start-api
npm run start-client
```

and it will run api at `http://localhost:8080`
and it will run clien at `http://localhost:3000`

![Scheme](client/public/logo193.png)
